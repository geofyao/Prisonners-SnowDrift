package com.learning;

public class Main {

    public static void main(String[] args) {
        int numSim=100;
        PrisonnersDilemma pdGame=null;
        for(int i=0;i<numSim;i++){
            pdGame= new PrisonnersDilemma();
            pdGame.launchGame();
        }
        pdGame.displayMeanResults(numSim);
    }
}
