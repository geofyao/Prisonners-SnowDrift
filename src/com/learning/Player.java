package com.learning;

/**
 * Created by Jey on 08/11/2016.
 */
public class Player {

    int points;
    static final int COOPERATE=1;
    static final int DEFECT=0;
    int lastAction=-1;
    int currentAction;
    Player[] neighbors;
    public Player(){
        currentAction= PrisonnersDilemma.random.nextInt(2);
    }
    public void setPoints(int points){
        this.points=points;
    }

    public void addPoints(int points){
        this.points+=points;
    }
    public void setLastAction(int lastAction){
        this.lastAction=lastAction;
    }
    public void setNeighbors(Player[] neighbors){
        this.neighbors=neighbors;
    }

    public void play(){
        this.points=0;
        if(currentAction==COOPERATE){
            if(!PrisonnersDilemma.results.containsKey(PrisonnersDilemma.run)){
                PrisonnersDilemma.results.put(PrisonnersDilemma.run,1);
            }else{
                PrisonnersDilemma.results.put(PrisonnersDilemma.run,PrisonnersDilemma.results.get(PrisonnersDilemma.run)+1);
            }
        }
        for (Player neighbor: neighbors ) {
            if(neighbor.currentAction==Player.COOPERATE){
                this.addPoints(this.currentAction==Player.COOPERATE? PrisonnersDilemma.R:PrisonnersDilemma.T);
            }
        }
        this.lastAction=this.currentAction;
    }

    public void copyBest(){
        Player best=this;
        for(int i=0;i<neighbors.length;i++){
            if(neighbors[i].points>best.points)
                best=neighbors[i];
        }
        this.currentAction=best.lastAction;
    }
}
