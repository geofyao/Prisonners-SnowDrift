package com.learning;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.TreeMap;

/**
 * Created by Jey on 08/11/2016.
 */
public class PrisonnersDilemma {
    static final int T=10;
    static final int R=7;
    static final int P=0;
    static final int S=0;
    static final int latticeSize=50;
    static int run=0;
    static TreeMap<Integer,Integer> results;
    public static Random random;
    List<List<Player>> lattice;
    public PrisonnersDilemma(){
        results=new TreeMap<Integer,Integer>();
        random= new Random(Instant.now().toEpochMilli());
        lattice=new ArrayList<>();
        for(int i=0;i<latticeSize;i++){
            List<Player> tmp= new ArrayList<>();
            for(int j=0;j<latticeSize;j++){
                tmp.add(new Player());
            }
            lattice.add(tmp);
        }
        setPlayersNeighbors();

    }

    public void launchGame(){
        run=0;
        for(int i=0;i<100;i++){
            pickPlayer();
            updateAction();
            run++;
        }

    }

    private void pickPlayer(){
        for(int i=0;i<latticeSize;i++){
            for(int j=0;j<latticeSize;j++){
                lattice.get(i).get(j).play();
            }
        }
    }

    private void setPlayersNeighbors(){

        for(int i=0;i<latticeSize;i++){
            for(int j=0;j<latticeSize;j++){
                Player[] neighbors= new Player[8];
                neighbors[0]=lattice.get((((i-1)%latticeSize)+latticeSize)%latticeSize).
                        get((((j-1)%latticeSize)+latticeSize)%latticeSize);
                neighbors[1]=lattice.get((((i-1)%latticeSize)+latticeSize)%latticeSize).
                        get((((j)%latticeSize)+latticeSize)%latticeSize);
                neighbors[2]=lattice.get((((i-1)%latticeSize)+latticeSize)%latticeSize).
                        get((((j+1)%latticeSize)+latticeSize)%latticeSize);
                neighbors[3]=lattice.get((((i)%latticeSize)+latticeSize)%latticeSize).
                        get((((j-1)%latticeSize)+latticeSize)%latticeSize);
                neighbors[4]=lattice.get((((i)%latticeSize)+latticeSize)%latticeSize).
                        get((((j+1)%latticeSize)+latticeSize)%latticeSize);
                neighbors[5]=lattice.get((((i+1)%latticeSize)+latticeSize)%latticeSize).
                        get((((j-1)%latticeSize)+latticeSize)%latticeSize);
                neighbors[6]=lattice.get((((i+1)%latticeSize)+latticeSize)%latticeSize).
                        get((((j)%latticeSize)+latticeSize)%latticeSize);
                neighbors[7]=lattice.get((((i+1)%latticeSize)+latticeSize)%latticeSize).
                        get((((j+1)%latticeSize)+latticeSize)%latticeSize);

                lattice.get(i).get(j).setNeighbors(neighbors);
            }
        }

    }

    private void updateAction(){
        for(int i=0;i<latticeSize;i++){
            for(int j=0;j<latticeSize;j++){
                lattice.get(i).get(j).copyBest();
            }
        }
    }
    public void displayResults(){
        System.out.println(results);
    }

    public void displayMeanResults(int numSim){
        for (Integer key: results.keySet()) {
            results.put(key,results.get(key)/numSim);
        }
        System.out.println(results);
    }



}
